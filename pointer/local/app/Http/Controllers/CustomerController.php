<?php namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Admins;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| User Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public $itemsPerPage = 5;
	
	
	public function __construct()
	{
		$this->middleware('auth');
		
		$db = new Admins();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	
	public function entire() {
		Session::put('search_user_name','');
		Session::put('search_user_kananame','');
		Session::put('search_usesr_email','');
		Session::put('admin_per_page',5);
		Session::put('admin_current_page',1);
		return $this->display();
	}
	
	public function nextPage() {
		return 1;
	}
	public function page_select(Request $request) {
		$page_num = $request->route('page');
		Session::put('admin_current_page', $page_num);
		return $this->display();
	}
	public function add() {
		return view('master.customer.add');
	}
	public function per_page(Request $request) {
		$per_page=$request->input('admin_per_page');
		Session::put('admin_per_page',$per_page);
		Session::put('admin_current_page',1);
		return $this->display();
	}
	public function insert(Request $request) {
		$data = array(
			'name' => $request->input('name'),
			'kananame' => $request->input('kananame'),
			'email' => $request->input('email'),
			'password' => bcrypt($request->input('password')),
			'confirm_code' => $request->input('confirmation')
		);
		
		$rule = array(
			'name' => "required|min:1|max:40",
			'kananame' => "required|min:1|max:40",
			'email'    => "required|min:2|max:128",
			'password' => "required|min:6|max:128",
			'confirm_code' => "required|min:1|max:255"
		);
		
 		$validator = Validator::make($data, $rule);
		
		if ($validator->fails()) {
			return redirect('/customer/add')->withInput()->withErrors($validator);
		}
		
		$db = new Admins();
		$db->insertNew($data);
		$result = Admins::all();
		
		return $this->display();
	}
	
	public function delete(Request $request) {
		$db = new Admins();
		$db->deleteAdmin($request->input('id'));
		$result = Admins::all();
		return $this->display();
	}
	
	public function manyDelete(Request $request) {
		$db = new Admins();
		$items = $request->input('items');
		
		$db->deleteAdmins($items);
		return 1;//because ajax	
	}
	
	public function modify(Request $request) {
		
		$format = "%Y-%m-%d %H:%M:%S";
		$currentTime = strftime($format);
		
		$itemId = $request->input('admin_id');
		$data = array(
			'username'=>$request->input('name'),
			'kananame'=>$request->input('kananame'),
			'email'=>$request->input('email'),
			'password'=>$request->input('password'),
			'confirm_code'=>$request->input('confirmation'),
			'logdate'=>$request->input('logdate'),
			'created'=>$request->input('created'),
			'modified'=>$currentTime
		);
		
		$db = new Admins();
		$db->updateAdmin($itemId, $data);
		$result = Admins::all();
		
		return $this->display();
	}
	
	public function search(Request $request) {
		$admin_name = $request->input("admin_name");
		$admin_kananame = $request->input("admin_kananame");
		$admin_email = $request->input("admin_email");
		
		Session::put('search_user_name',$admin_name);
		Session::put('search_user_kananame',$admin_kananame);
		Session::put('search_usesr_email',$admin_email);
		Session::put('admin_current_page',1);
		return $this->display();
	}
	public function display(){
		$name=Session::get('search_user_name');
		$kananame=Session::get('search_user_kananame');
		$email=Session::get('search_usesr_email');
		$per_page=Session::get('admin_per_page');
		$current_page=Session::get('admin_current_page');
	
		$admin_search_data=Admins::whereRaw('name like ? and kananame like ? and password like ?',["%".$name."%","%".$kananame."%","%".$email."%"])->get();

		$total=Admins::count();
		$selected_num=$admin_search_data->count();
		if ($selected_num==0) {
			$current_page=0;
		}elseif ($current_page==0){
			$current_page=1;
		}
		Session::put('admin_current_page',$current_page);
		$from_page=$selected_num==0?0:1;
		$to_page=ceil($selected_num/$per_page);
		
		$url='admin/page_sel';
		$first_page_url=url($url,array('page'=>$from_page));
		$last_page_url=url($url,array('page'=>$to_page));
	
		if ($current_page<2){
			$prev_page_url=url($url,array('page'=>$current_page));
		}else{
			$prev_page_url=url($url,array('page'=>($current_page-1)));
		}
		if ($current_page==$to_page){
			$next_page_url=url($url,array('page'=>$current_page));
		}else{
			$next_page_url=url($url,array('page'=>($current_page+1)));
		}
		if ($current_page>0) {
			$skip=($current_page-1)*$per_page;
		}else{
			$skip=0;
		}
		$admin_data=Admins::whereRaw('name like ? and kananame like ? and password like ?',["%".$name."%","%".$kananame."%","%".$email."%"])->skip($skip)->take($per_page)->get();
	
		$data=$admin_data->toArray();
		$admin_data=array('admin_name'=>$name,
				'kananame'=>$kananame,
				'email'=>$email,
				'per_page'=>$per_page,
				'current_page'=>$current_page,
				'first_page_url'=>$first_page_url,
				'last_page_url'=>$last_page_url,
				'prev_page_url'=>$prev_page_url,
				'next_page_url'=>$next_page_url,
				'total'=>$total,
				'selected_num'=>$selected_num,
				'from_page'=>$from_page,
				'to_page'=>$to_page,
				'data'=>$data,
		);
		return view('master.customer.index')->with("admin_data", $admin_data);
	}
}
