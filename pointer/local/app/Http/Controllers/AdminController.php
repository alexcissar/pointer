<?php namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Admins;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| User Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	public function __construct()
	{
		//$this->middleware('auth');
		
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	
	public function entire() {
		
		
		$result=Admins::all();
		return view('master.admin',compact('result'));
		
	}
	
	public function insert(Request $request) {
		
		$provider = "新運営者の情報を正しい入力してください";
				
		$data = array(
			'name' => $request->input('name'),
			'kananame' => $request->input('kananame'),
			'email' => $request->input('email'),
			'password' => md5($request->input('password')),
			'confirm_code' => $request->input('confirmation') 
		);
		
		$rule = array(
			'name' => "required|min:1|max:40",
			'kananame' => "required|min:1|max:40",
			'email'    => "required|min:2|max:128",
			'password' => "required|min:3|max:128",
			'confirm_code' => "required|min:1|max:255"
		);
		
// 		$validation = Validator::make($data,$rule);
		
// 		if ($validation->fails()) {
// 			return redirect()->back()->withErrors($provider);
// 		}
		
		$db = new Admins();
		$db->insertNew($data);
		$result = Admins::all();
		
		//return view('master.admin',compact('result'));
		return redirect()->back();
	}
	
	public function delete(Request $request) {
		$db = new Admins();
		$db->deleteAdmin($request->input('id'));
		$result = Admins::all();
		
		//return view('master.admin',compact('result'));
		return redirect()->back();

	}
	
	public function manyDelete(Request $request) {
		$db = new Admins();
		$items = $request->input('items');
		
		$db->deleteAdmins($items);
		return 1;//because ajax	
	}
	
	public function modify(Request $request) {
		
		$format = "%Y-%m-%d %H:%M:%S";
		$currentTime = strftime($format);
		
		$itemId = $request->input('admin_id');
		$data = array(
			'username'=>$request->input('name'),
			'kananame'=>$request->input('kananame'),
			'email'=>$request->input('email'),
			'password'=>$request->input('password'),
			'confirm_code'=>$request->input('confirmation'),
			'logdate'=>$request->input('logdate'),
			'created'=>$request->input('created'),
			'modified'=>$currentTime
		);
		$db = new Admins();
		$db->updateAdmin($itemId, $data);
		$result = Admins::all();
		//return view('master.admin',compact('result'));
		return redirect()->back();
	}
	
}
