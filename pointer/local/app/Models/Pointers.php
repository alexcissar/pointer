<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Pointers extends Model {

	protected $table = "pointer";
	protected $fillable = ['id','group_id','pointer_age','pointer_period','category_id','pointer_name','pointer_kananame','pointer_address','pointer_longitude','pointer_latitude','pointer_curaddress','pointer_comment','pointer_extra','pointer_show','created','modified'];
	
	public function __construct() {
		
	}
	
	public function allPointers() {
		
		return DB::table($this->table)->join('category','pointer.category_id','=','category.id')->select('pointer.id','pointer.pointer_name','pointer.pointer_curaddress','category.category_name')->get();
		
	}
	
	public function getPointers($pt_name, $pt_text, $category_id, $age) {
		
		if ($category_id == 0) {
			//$res = DB::table($this->table)->whereRaw('pointer_name like ? and pointer_age = ? and pointer_comment like ?',["%".$pt_name."%",$age,"%".$pt_text."%"])->get();
			$res = DB::table($this->table)->join('category','pointer.category_id','=','category.id')->select('pointer.id','pointer.pointer_name','pointer.pointer_curaddress','category.category_name')->whereRaw('pointer_name like ? and pointer_age = ? and pointer_comment like ?',["%".$pt_name."%",$age,"%".$pt_text."%"])->get();
			return $res;
		}else {
			//$res = DB::table($this->table)->whereRaw('pointer_name like ? and pointer_age = ? and pointer_comment like ? and category_id = ?',["%".$pt_name."%",$age,"%".$pt_text."%",$category_id])->get();
			$res = DB::table($this->table)->join('category','pointer.category_id','=','category.id')->select('pointer.id','pointer.pointer_name','pointer.pointer_curaddress','category.category_name')->whereRaw('pointer_name like ? and pointer_age = ? and pointer_comment like ? and category_id=?',["%".$pt_name."%",$age,"%".$pt_text."%",$category_id])->get();
			return $res;
		}
	
	}
	
}