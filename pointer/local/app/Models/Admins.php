<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Admins extends Model {
	
	protected $table = "admin_user";
	protected $fillable = ['name', 'kananame','email', 'password','confirm_code','logdate','created_at','updated_at'];
	protected $guarded = ['id','password'];
	
	public function __construct() {
		
	}
	public function insertNew($data) {
		
		DB::table($this->table)->insert($data);
	}
	
	public function deleteAdmin($id) {
		
		DB::table($this->table)->where('id','=',$id)->delete();
	}
	
	public function deleteAdmins($ids) {
		
		foreach ($ids as $item) {
			DB::table($this->table)->where('id','=',$item)->delete();
		}
	}
	
	public function updateAdmin($id, $data) {
		
		DB::table($this->table)->where('id','=',$id)->update([
			'name'=>$data['username'],
			'kananame'=>$data['kananame'],
			'email'=>$data['email'],
			'password'=>md5($data['password']),
			'confirm_code'=>$data['confirm_code'],
			'logdate'=>$data['logdate'],
			'created_at'=>$data['created'],
			'updated_at'=>$data['modified']
		]);
	}
	
	public function searchAdminUsers($data) {
		$admin_name = $data['name'];
		$admin_kananame = $data['kananame'];
		$admin_email = $data['email'];
		$itemsPerPage = $data['itemsPerPage'];
		
		$queryString = "select * from admin_user where name like ? and kananame like ? and email like ?";
		
		//$result = User::select($queryString, ['%'.$admin_name.'%', '%'.$admin_kananame.'%', '%'.$admin_email.'%']);
		$result = DB::table($this->table)->select($queryString, ['%'.$admin_name.'%', '%'.$admin_kananame.'%', '%'.$admin_email.'%']);//->paginate($itemsPerPage);
		//$result = DB::table($this->table)->select($queryString, [1]);//->paginate($itemsPerPage);
		echo count($result); exit();
		echo "66666666"; exit();
		return $result;
	}
	
}