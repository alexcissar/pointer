@extends('app') @section('content')
<link rel="stylesheet" href="{{asset('css/pointer/pointer.css')}}" />

<div class="breadcrumbs" id="breadcrumbs">
	<script type="text/javascript">
		try { ace.settings.check('breadcrumbs', 'fixed'); } catch (e) { }
	</script>

	<ul class="breadcrumb">
		<li>
			<i class="icon-home home-icon"></i>
			<a href="#">マスター管理</a>
		</li>
		<li class="active">ポインタ管理</li>
	</ul><!-- .breadcrumb -->
</div>

<div id="map"></div>
<div class="right_side">
	<div>
	<label for="sidebar_age"
		class=" control-label "> Age </label> 
	<input	type="text" class=" " placeholder="age"	id="sidebar_age">
	</div>
	<div>
	<label for="sidebar_period"
		class=" control-label "> Period </label> 
	<input	type="text" class=" " placeholder="period"	id="sidebar_period">
	</div>
	<div>
	<label for="sidebar_pointer_name"
		class=" control-label "> Pointer name </label> 
	<input	type="text" class=" " placeholder="pointer name"	id="sidebar_pointer_name">
	</div>
	<div>
	<label for="sidebar_kana_name"
		class=" control-label "> Kana name </label> 
	<input	type="text" class=" " placeholder="kana name"	id="sidebar_kana_name">
	</div>
	<div>
	<label for="sidebar_position"
		class=" control-label "> Position </label> 
	<input	type="text" class=" " placeholder="position"	id="sidebar_position">
	</div>
	<div>
	<label for="sidebar_current_address"
		class=" control-label "> Current address </label> 
	<input	type="text" class=" " placeholder="current address"	id="sidebar_current_address">
	</div>
	<div>
	<label for="sidebar_cont" class=" control-label ">Content</label>
		<textarea placeholder="content" id="sidebar_cont" class="" style="width: 281px; height: 93px;"></textarea>
	</div>
	<div>
	<label for="sidebar_extra" class=" control-label ">Extra</label>
		<textarea placeholder="extra" id="sidebar_extra" class="" style="width: 281px; height: 93px;"></textarea>
	</div>
			
		
</div>
<input />
</div>
<div class="bottom_side"></div>
<script src="{{asset('js/openlayer/OpenLayers.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	var map, layer;
	var mapBounds = new OpenLayers.Bounds(0.000000, -5575.000000, 7500.000000, 0.000000);
	var mapMinZoom = 0;
	var mapMaxZoom = 5;
	var mapMaxResolution = 1.000000;
	var gridBounds = new OpenLayers.Bounds(0.000000, -5575.000000, 7500.000000, 0.000000);
	var options = {
	    controls: [],
	    maxExtent : gridBounds,
	    minResolution: mapMaxResolution,
	    numZoomLevels: mapMaxZoom+1
	  };
	  map = new OpenLayers.Map('map', options);
	  layer = new OpenLayers.Layer.XYZ( "MapTiler layer", "${z}/${x}/${y}.png", {
	    transitionEffect: 'resize',
	    tileSize: new OpenLayers.Size(256, 256),
	    tileOrigin: new OpenLayers.LonLat(gridBounds.left, gridBounds.top)
	  });
	  map.addLayer(layer);
	  map.zoomToExtent(mapBounds);
	  map.addControl(new OpenLayers.Control.Navigation());
	  map.addControl(new OpenLayers.Control.PanZoom());
	  map.addControl(new OpenLayers.Control.MousePosition({  numDigits: 0 }));
	  map.addControl(new OpenLayers.Control.Permalink());
}
</script>
@endsection

