@extends('app') 

@section('content')

<div class="page-content">
	<div class="page-header">
		<h1>運営者管理</h1>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="alert alert-block alert-success">
				<i id="warning-pan" class="icon-ok green"></i>
				<span id="warning-content" class="text-success">おはようございます。</span>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="widget-box">
				<div class="widget-header widget-header-flat widget-header-small">
					<h5>
						<i class="icon-table"></i> 登録した運営者たち
					</h5>
					<div class="widget-toolbar no-border action-buttons">
						
						<a class="blue" href="#modal-form" id="admin_insert_btn"
							role="button" data-toggle="modal"> <i
							class="icon-plus-sign bigger-120"></i>
						</a>
						<a class="red" href="#many_delete_alert_form" id="many_delete_btn"
							role="button" data-toggle="modal"> <i
							class="icon-trash bigger-120"></i>
						</a>
					</div>
				</div>
				<div class="widget-body">
					<div class="widget-main no-padding dataTables_wrapper">
						<form>
							<table
								class="table table-bordered table-striped table-hover dataTable"
								id="sample-table-2">
								<thead>
									<tr role="row">
										<th class="center sorting_disabled" role="columnheader"
											rowspan="1" colspan="1" aria-label=" "><label> <input
												class="ace all_check" type="checkbox" /> <span class="lbl"></span>
										</label></th>
										<th role="columnheader" tabindex="0"
											aria-controls="sample-table-2" rowspan="1" colspan="1">No</th>
										<th role="columnheader" tabindex="0"
											aria-controls="sample-table-2" rowspan="1" colspan="1">運営者名</th>
										<th role="columnheader" tabindex="0"
											aria-controls="sample-table-2" rowspan="1" colspan="1">カナ名</th>
										<th role="columnheader" tabindex="0"
											aria-controls="sample-table-2" rowspan="1" colspan="1">ログインID</th>

										<th role="columnheader" tabindex="0"
											aria-controls="sample-table-2" rowspan="1" colspan="1">最後ログイン時間</th>
										<th role="columnheader" tabindex="0"
											aria-controls="sample-table-2" rowspan="1" colspan="1">登録時間</th>
										<th role="columnheader" tabindex="0"
											aria-controls="sample-table-2" rowspan="1" colspan="1">更新時間</th>
										<th role="columnheader" tabindex="0"
											aria-controls="sample-table-2" rowspan="1" colspan="1">active</th>
									</tr>
								</thead>
								<tbody>
								<?php $index = 0;?>
								<?php foreach ($result as $admin) {?>
								<?php $index = $index + 1;?>
								<tr>
										<td class="center sorting_1"><label> <input
												class="ace many_check" type="checkbox"
												many_item_id="<?php echo $admin->id?>" /> <span class="lbl"></span>
										</label></td>
										<td><?php echo $index;?></td>
										<td class="name<?php echo $admin->id?>"><?php echo $admin->name;?></td>
										<td class="kana<?php echo $admin->id?>"><?php echo $admin->kananame;?></td>
										<td class="email<?php echo $admin->id?>"><?php echo $admin->email;?></td>
										<td class="logdate<?php echo $admin->id?>"><?php echo $admin->logdate;?></td>
										<td class="created<?php echo $admin->id?>"><?php echo $admin->created_at;?></td>
										<td class="modified<?php echo $admin->id?>"><?php echo $admin->updated_at;?></td>
										<td>
											<div class="pull-right action-buttons" id="active_control">
												<a class="blue modify_ok" href="#modify_alert_form"
													id="modify_con" role="button" data-toggle="modal"
													item_id="<?php echo $admin->id;?>"> <i
													class="icon-pencil bigger-130"></i>
												</a> <span class="vbar"></span> <a class="red delete_ok"
													href="#delete_alert_form" id="delete_con" role="button"
													data-toggle="modal" item_id="<?php echo $admin->id;?>"> <i
													class="icon-trash bigger-130"></i>
												</a> <span class="vbar"></span>
											</div>
										</td>
									</tr>
									<input type="hidden"
										class="confirmation<?php echo $admin->id?>"
										value="<?php echo $admin->confirm_code?>">
								<?php }?>
							</tbody>
							</table>
							<div class="row">
								<div class="col-sm-10"></div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modal-form" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">新運営者の情報を入力してください</h4>
			</div>

			<div class="modal-body overflow-visible">
				<form class="form-horizontal" role="form" method="post"
					action="{{url('master/insert')}}">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="name-field">運営者名</label>
						<div class="col-sm-9">
							<input type="text" class="col-xs-10 col-sm-8" id="name-field"
								name="name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="kananame-field">運営者カナ名</label>
						<div class="col-sm-9">
							<input type="text" class="col-xs-10 col-sm-8" id="kananame-field"
								name="kananame">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="email-field">ログインID</label>
						<div class="col-sm-9">
							<input type="text" class="col-xs-10 col-sm-6" id="email-field"
								name="email">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="password-field">パスワード</label>
						<div class="col-sm-9">
							<input type="password" class="col-xs-10 col-sm-7"
								id="password-field" name="password">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="confirm-field">パスワード確認</label>
						<div class="col-sm-9">
							<input type="password" class="col-xs-10 col-sm-7"
								id="confirm-field">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="confirmation-field">確認コード</label>
						<div class="col-sm-9">
							<textarea rows="5" cols="15" class="col-xs-10 col-sm-7"
								id="confirmation-field" name="confirmation"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="icon-remove"></i> 打ち消し
						</button>

						<button class="btn btn-sm btn-primary" type="submit">
							<i class="icon-ok"></i> 保管
						</button>
					</div>
				</form>
				
			</div>
		</div>
	</div>
</div>
<div id="modify_alert_form" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">運営者の情報を入力してください</h4>
			</div>

			<div class="modal-body overflow-visible">
				<form class="form-horizontal" role="form" method="post"
					action="{{url('master/modify')}}">

					<input type="hidden" name="_token" value="{{csrf_token()}}"> <input
						type="hidden" id="modify_id" value="" name="admin_id">
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="modify-name-field">運営者名</label>
						<div class="col-sm-9">
							<input type="text" class="col-xs-10 col-sm-8"
								id="modify-name-field" name="name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="modify-kananame-field">運営者カナ名</label>
						<div class="col-sm-9">
							<input type="text" class="col-xs-10 col-sm-8"
								id="modify-kananame-field" name="kananame">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="modify-email-field">ログインID</label>
						<div class="col-sm-9">
							<input type="text" class="col-xs-10 col-sm-6"
								id="modify-email-field" name="email">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="modify-password-field">パスワード</label>
						<div class="col-sm-9">
							<input type="password" class="col-xs-10 col-sm-7"
								id="modify-password-field" name="password">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="modify-confirm-field">パスワード確認</label>
						<div class="col-sm-9">
							<input type="password" class="col-xs-10 col-sm-7"
								id="modify-confirm-field">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="modify-confirmation-field">確認コード</label>
						<div class="col-sm-9">
							<textarea rows="5" cols="15" class="col-xs-10 col-sm-7"
								id="modify-confirmation-field" name="confirmation"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="modify-logdate-field">最後ログイン時間</label>
						<div class="col-sm-9">
							<input type="text" class="col-xs-10 col-sm-6 input-mask-date"
								id="modify-logdate-field" name="logdate">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="modify-created-field">登録時間</label>
						<div class="col-sm-9">
							<input type="text" class="col-xs-10 col-sm-6"
								id="modify-created-field" name="created">
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal">
							<i class="icon-remove"></i> 打ち消し
						</button>

						<button class="btn btn-sm btn-primary" type="submit">
							<i class="icon-ok"></i> 保管
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="delete_alert_form" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">このかたの情報をのこと削除しますか</h4>
			</div>
			<div class="modal-footer">
				<form method="post" action="{{url('master/delete')}}">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<button class="btn btn-sm btn" data-dismiss="modal">
						<i class="icon-remove"></i>打ち消し
					</button>
					<button class="btn btn-sm btn-primary" id="delete_ok_btn"
						type="submit">
						<i class="icon-ok"></i>削除
					</button>
					<input type="hidden" name="id" id="id_store" value="">
				</form>

			</div>
		</div>
	</div>
</div>

<div id="many_delete_alert_form" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">この運営者たちの情報をのこと削除しますか</h4>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn" data-dismiss="modal">
					<i class="icon-remove"></i>打ち消し
				</button>
				<button class="btn btn-sm btn-primary" id="many_delete_ok_btn">
					<i class="icon-ok"></i>削除
				</button>
			</div>
		</div>
	</div>
</div>

<script src="{{asset('js/ace/jquery-2.0.3.min.js')}}"></script>
<script src="{{asset('js/ace/bootstrap.min.js')}}"></script>

<!-- ace scripts -->

<script src="{{asset('js/ace/ace-elements.min.js')}}"></script>

<script src="{{asset('/js/ace/jquery.maskedinput.min.js')}}"></script>
<!-- inline scripts related to this page -->

<script type="text/javascript">
	jQuery(function($) {

		var many_delete_url = $("input#many_delete_url").attr('value');
		
		$("table input:checkbox").attr("checked",false);

		$.mask.definitions['~']='[+-]';
		$("input.input-mask-date").mask("xxxx-xx-xx xx:xx:xx");
		
		$('table th input:checkbox').on('click' , function(){
			
			var that = this;
			$(this).closest('table').find('tr > td:first-child input:checkbox')
			.each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});
				
		});
		
		$("#modal-form").on('shown.bs.modal',function() {
			$("input#name-field").focus();
		});
				
		
		$(".delete_ok").bind('click',function() {
			var id = $(this).attr('item_id');
			$("#id_store").val(id);
		});

		$(".modify_ok").bind('click', function() {
			var id = $(this).attr('item_id');
			$("#modify_id").val(id);
			$("input#modify-name-field").val($("td.name"+id).text());
			$("input#modify-kananame-field").val($("td.kana"+id).text());
			$("input#modify-email-field").val($("td.email"+id).text());
			$("input#modify-password-field").val("");
			$("input#modify-confirm-field").val("");
			$("textarea#modify-confirmation-field").text($("input.confirmation"+id).val());
			$("input#modify-logdate-field").val($("td.logdate"+id).text());
			$("input#modify-created-field").val($("td.created"+id).text());
		});

		$("button#many_delete_ok_btn").bind('click',function() {
			var item_array = new Array();
			$("input.many_check:checked").each(function() {
				item = $(this).attr("many_item_id");
				item_array.push(item);
			});
			$.ajax({
				headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type:"post",
				dataType:"json",
				url:"{{url('master/deletes')}}",
				data:{'items':item_array},
				success:function(json) {
					location.reload();
				}
			});
			
		});
		
	});
</script>

@endsection
