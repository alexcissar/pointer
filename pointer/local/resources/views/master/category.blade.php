@extends('app')
@section('content')

<link rel="stylesheet" href="{{asset('css/master/category.css')}}" />

<div class="breadcrumbs" id="breadcrumbs">
	<script type="text/javascript">
		try { ace.settings.check('breadcrumbs', 'fixed'); } catch (e) { }
	</script>

	<ul class="breadcrumb">
		<li>
			<i class="icon-home home-icon"></i>
			<a href="#">マスター管理</a>
		</li>
		<li class="active">カテゴリ管理</li>
	</ul><!-- .breadcrumb -->
</div>

<div class="page-content">
	<div class="page-header">
		<h1>カテゴリ管理</h1>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="widget-box">
				<div class="widget-header widget-header-small">
					<h5 class="lighter">カテゴリ検索</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main">
					<form role="form" method="POST" class="form-search"  action="{{ url('category/search') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="row">
								<div class="col-xs-12 col-sm-8">
									<div class="input-group" id="input_category_div">
										<label for="category_search_input">カテゴリ名</label>
										<input id="category_search_input" type="text" 
										name="category_search_name" value="<?php echo $category_data['search_category_name']?>"
										placeholder="category name" class="search-query">
										<span class="input-group-btn">
											<button class="btn btn-purple btn-sm" type="submit">
												検　索
												<i class="icon-search icon-on-right bigger-110"></i>
											</button>
										</span>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="widget-box">
				<div class="alert alert-block alert-success">
					検索結果:
					<strong class="green">
						<?php echo $category_data['total']?> <small>件中</small>
						<small><?php echo $category_data['selected_num']?></small>
					</strong>
					    件が見つかりました。(<?php
					     if ($category_data['current_page']==0) {
					     	$from=0;
					     }else{
					   		 $from= (($category_data['current_page']-1)*$category_data['per_page']+1);
					    }
					    if ($category_data['per_page']>$category_data['selected_num']) {
					    	$to=$category_data['selected_num'];
					    }else{
					    $to=($category_data['current_page']*$category_data['per_page'])>$category_data['total']?$category_data['total']:($category_data['current_page']*$category_data['per_page']);
					    }
					    echo $from.'~'.$to;
					    ?>)
				</div>
				<div class="message-footer clearfix">
					<div class="pull-left"> 
					<a data-toggle="modal" id="new_category" class="blue" role="button" href="#create-modal-form">
					<i class="icon-plus-sign bigger-120"></i> </a>
					<a data-toggle="modal" id="delete_category" class="red" role="button" href="#delete-some-confirm">
					<i class="icon-trash bigger-120"></i> </a>
					 </div>

						<div class="pull-right">
							<form id="per_page_form" name="frm_page" method="post" action="{{ url('category/per_page')}}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="per_page_num" value="5" id="page_num_id"/>
                                        <select style="min-width:80px" class="sel_per_page" >
                                           	<option selected="" id="per_page5">5</option>
                                            <option  id="per_page10">10</option>
                                            <option  id="per_page20">20</option>
                                            <option  id="per_page30">30</option>
                                            <option  id="per_page50">50</option>
                                        </select>
                             </form>
							<div class="inline middle"> page <?php echo $category_data['current_page']?> of <?php echo $category_data['to_page']?> </div>
                            
							&nbsp; &nbsp;
							<ul class="pagination middle">
								<li>
									<a href="<?php echo $category_data['first_page_url']; ?>"">
										<i class="icon-step-backward middle"></i>
									</a>
								</li>

								<li>
									<a href="<?php echo $category_data['prev_page_url']; ?>">
										<i class="icon-caret-left bigger-140 middle"></i>
									</a>
								</li>

								<li>
									<a href="#">
										<input type="text" maxlength="3" class="input-mask-product" value="<?php echo $category_data['current_page']?>">
									</a>
								</li>

								<li>
									<a href="<?php echo $category_data['next_page_url']; ?>">
										<i class="icon-caret-right bigger-140 middle"></i>
									</a>
								</li>

								<li>
									<a href="<?php echo $category_data['last_page_url']; ?>">
										<i class="icon-step-forward middle"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>

				<div class="widget-body">
					<div class="widget-main no-padding dataTables_wrapper">
						<table class="table table-bordered table-striped table-hover dataTable" id="sample-table-2">
							<thead>
								<tr role="row">
									<th class="center sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label=" ">
										<input class="delete_muti_checkbox ace" type="checkbox" />
										<span class="lbl"></span>
									</th>
									<th role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1">No</th>
									<th role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" aria-label="Domain: active to sort column ascending">カテゴリ名</th>
									<th role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1">登録時間</th>
									<th role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1"></i>更新時間</th>
									<th role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1">詳細</th>
								</tr>
							</thead>
							<tbody>
								<?php  $index = $from-1;?>
								<?php  foreach ($category_data['data'] as $value) {?>
								<?php  $index = $index + 1;?>
								<tr>
									<td class="center sorting_1">
										<label>
											<input class="delete_muti_checkbox ace" type="checkbox" item_id="<?php echo $value["id"];?>"/>
											<span class="lbl"></span>
										</label>
									</td>
									<td><?php  echo $index;?></td>
									<td class="category_name_td"><?php  echo $value['category_name'] ;?></td>
									<td><?php  echo $value['created'];?></td>
									<td><?php  echo $value['modified'];?></td>
									<td class="center">
										<div class="pull-center action-buttons" id="active_control">
											<a class="blue edit_category" data-toggle="modal" item_id="<?php echo $value["id"];?>" category_name="<?php echo $value["category_name"];?>" role="button" href="#edit-modal-confirm" id="delete_con">
												<i class="icon-pencil bigger-130"></i>
											</a>
											<span class="vbar"></span>
											
											<a class="red delete_one_category" data-toggle="modal" item_id="<?php echo $value["id"];?>" role="button" href="#delete-one-confirm" id="delete_con">
												<i class="icon-trash bigger-130"></i>
											</a>
											<span class="vbar"></span>
										</div>
									</td>
								</tr>
								<?php  }?>
							</tbody>
						</table>
						<div class="row">
							<div class="col-sm-10"></div>
						</div>
					</div>
				</div>
				<div class="message-footer clearfix">
					<div class="pull-left"> 
					<a data-toggle="modal" id="new_category" class="blue" role="button" href="#create-modal-form">
					<i class="icon-plus-sign bigger-120"></i> </a>
					<a data-toggle="modal" id="delete_category" class="red" role="button" href="#delete-some-confirm">
					<i class="icon-trash bigger-120"></i> </a>
					 </div>

						<div class="pull-right">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="per_page_num" value="5" id="page_num_id"/>
                                        <select style="min-width:80px" class="sel_per_page" >
                                           	<option selected="" class="per_page5">5</option>
                                            <option  class="per_page10">10</option>
                                            <option  class="per_page20">20</option>
                                            <option  class="per_page30">30</option>
                                            <option  class="per_page50">50</option>
                                        </select>
							<div class="inline middle"> page <?php echo $category_data['current_page']?> of <?php echo $category_data['to_page']?> </div>
                            
							&nbsp; &nbsp;
							<ul class="pagination middle">
								<li>
									<a href="<?php echo $category_data['first_page_url']; ?>"">
										<i class="icon-step-backward middle"></i>
									</a>
								</li>

								<li>
									<a href="<?php echo $category_data['prev_page_url']; ?>">
										<i class="icon-caret-left bigger-140 middle"></i>
									</a>
								</li>

								<li>
									<a href="#">
										<input type="text" maxlength="3" class="input-mask-product" value="<?php echo $category_data['current_page']?>">
									</a>
								</li>

								<li>
									<a href="<?php echo $category_data['next_page_url']; ?>">
										<i class="icon-caret-right bigger-140 middle"></i>
									</a>
								</li>

								<li>
									<a href="<?php echo $category_data['last_page_url']; ?>">
										<i class="icon-step-forward middle"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>
<div id="create-modal-form" class="modal" tabindex="-1">
									<div class="modal-dialog ">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">新カテゴリ名を入力してください</h4>
											</div>

											<div class="modal-body overflow-visible">
												<div class="row">

													<div class="col-xs-12 col-sm-12">
														<div class="form-group">
															<label for="form-field-1" class="col-sm-4 control-label no-padding-right"> カテゴリ名 </label>
					
															<div class="col-sm-8">
																<input type="text" class="col-xs-10 col-sm-12 create_category_name" placeholder="category" name="category_name">
																<h5 class="create_error">
																	Same category name already exists!
																</h5>
																<h5 class="create_success">
																	Successfully inserted,go to last page to confirm!
																</h5>
															</div>
														</div>
													</div>
												</div>
											</div>

											<div class="modal-footer">
												<button class="btn btn-sm" data-dismiss="modal">
													<i class="icon-remove"></i>
													打ち消し
												</button>
												<button type="submit" class="btn btn-sm btn-primary create_category_btn" >
													<i class="icon-ok"></i>
													保管
												</button>
											</div>
										</div>
									</div>
								</div><!-- PAGE CONTENT ENDS -->
		<div id="delete-one-confirm" class="modal" tabindex="-1">
									<div class="modal-dialog ">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">Warning!</h4>
											</div>
											<form role="form" method="POST" action="{{ url('/category/delete_one') }}">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<input type="hidden" id="category_id" name="item_id" value="">
											<div class="modal-body overflow-visible">
												<div class="row">

													<div class="col-xs-12 col-sm-12">
														<div class="form-group">
															<label style="font-size:1.5em; color:#FF9A9A" class="col-sm-12 control-label no-padding-right"> このカテゴリをのこと削除しますか </label>
														</div>
													</div>
												</div>
											</div>

											<div class="modal-footer">
												<button class="btn btn-sm" data-dismiss="modal">
													<i class="icon-remove"></i>
													打ち消し
												</button>

												<button type="submit" class="btn btn-sm btn-primary delete_one_category_btn" >
													<i class="icon-ok"></i>
													削除
												</button>
											</div>
											</form>
										</div>
									</div>
								</div><!-- PAGE CONTENT ENDS -->
	<div id="edit-modal-confirm" class="modal" tabindex="-1">
									<div class="modal-dialog ">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">下記の項目を入力し、『入力した内容で確認』をクリックして下さい。</h4>
											</div>
											<input type="hidden" id="edit_category_id" name="item_id" value="">
											<div class="modal-body overflow-visible">
												<div class="row">

													<div class="col-xs-12 col-sm-12">
														<div class="form-group">
															<label for="form-field-1" class="col-sm-4 control-label no-padding-right"> カテゴリ名 </label>
					
															<div class="col-sm-8">
																<input type="text" class="col-xs-10 col-sm-12 update_category_name" id="edit_category_default" placeholder="category" name="category_name">
																<h5 class="update_error">
																	Same category name already exists!
																</h5>
																<h5 class="update_success">
																	Successfully updated!
																</h5>
															</div>
														</div>
													</div>
												</div>
											</div>

											<div class="modal-footer">
												<button class="btn btn-sm" data-dismiss="modal">
													<i class="icon-remove"></i>
													打ち消し
												</button>

												<button type="submit" class="btn btn-sm btn-primary update_category_btn" >
													<i class="icon-ok"></i>
													クリア
												</button>
											</div>
										</div>
									</div>
								</div><!-- PAGE CONTENT ENDS -->
<div id="delete-some-confirm" class="modal" tabindex="-1">
									<div class="modal-dialog ">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">Warning!</h4>
											</div>
											<input type="hidden" id="category_id" name="item_id" value="">
											<div class="modal-body overflow-visible">
												<div class="row">

													<div class="col-xs-12 col-sm-12">
														<div class="form-group">
															<label style="font-size:1.5em; color:#FF9A9A" class="col-sm-12 control-label no-padding-right"> Do you really want to erase all the checked items? </label>
														</div>
													</div>
												</div>
											</div>

											<div class="modal-footer">
												<button class="btn btn-sm" data-dismiss="modal">
													<i class="icon-remove"></i>
													打ち消し
												</button>

												<button type="submit" class="btn btn-sm btn-primary delete_some_category_btn" >
													<i class="icon-ok"></i>
													削除
												</button>
											</div>
										</div>
									</div>
								</div><!-- PAGE CONTENT ENDS -->
		<input type="hidden" id="url" value="{{url('category/create')}}"/>
		<script src="{{asset('js/ace/jquery-2.0.3.min.js')}}"></script>
		<script src="{{asset('js/ace/bootstrap.min.js')}}"></script>
		<script src="{{asset('js/ace/fuelux/fuelux.spinner.min.js')}}"></script>
		<script src="{{asset('js/ace/jquery.maskedinput.min.js')}}"></script>
		<script type="text/javascript">
		
		  $(document).ready(function() {
	
			var per_page = parseInt($.trim("<?php echo $category_data['per_page']; ?>"));
			  
			  $(".delete_one_category").click(function(){
				  var name=$(this).attr('item_id');
				  $("#category_id").val(name);
			  });
			  $(".edit_category").click(function(){
				  var name=$(this).attr('item_id');
				  $("#edit_category_id").val(name);
				  $("#edit_category_default").val($(this).attr('category_name'));
			  });
			  $(".update_form").bind("submit", function(event){
				  var name=$(".update_category_name").val();
				  if ($.trim(name).length>0){
					  $(".update_form").submit();
				  }else{
					  event.preventDefault();
				  }
				  
				 });
			 $(".btn-sm").click(function(){
				 $(".create_error").hide();
				 $(".create_success").hide();
				 $(".update_error").hide();
				 $(".update_success").hide();
			 });
			 $(".create_category_btn").click(function(){
				 var cname=$(".create_category_name").val();
				 $.ajax({
					  	headers: {
				              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				        },
				        type:"post",
						url: "{{ url('category/create') }}",
		          		dataType: 'json',
						data: { 'category_name' : cname },
						success: function(json) {					
							if(json==1){
								$(".create_error").show();
							    $(".create_success").hide();
							}else{
								$(".create_success").show();
								$(".create_error").hide();
							}
						}
				   })
			 });
			 $(".update_category_btn").click(function(){
				 var cname=$(".update_category_name").val();
				 var id=$("#edit_category_id").val();
				 $.ajax({
					  	headers: {
				              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				        },
				        type:"post",
						url: "{{ url('category/update') }}",
		          		dataType: 'json',
						data: { 'category_name' : cname ,'item_id':id},
						success: function(json) {					
							if(json==1){
								$(".update_error").show();
							    $(".update_success").hide();
							}else{
								$(".update_success").show();
								$(".update_error").hide();
								$("td div a[item_id="+id+"]").parents('tr').children(".category_name_td").text(cname);
								$("td div a[item_id="+id+"]").parents('tr').children("td:last-child").children('div').children('.edit_category').attr('category_name',cname);
							}
						}
				   })
			 });
			 
			  $("table input:checkbox").attr("checked",false);
			  $(".delete_some_category_btn").click(function(){
				  var item_array =new Array();
				  $(".delete_muti_checkbox:checked").each(function(i){
					  temp=$(this).attr('item_id');
					  item_array.push(temp);
				  });
				  $.ajax({
					  	headers: {
				              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				        },
				        type:"post",
						url: "{{ url('category/delete_some') }}",
		          		dataType: 'json',
						data: { 'items' : item_array },
						success: function(json) {					
							location.reload();
						}
				   })
			  });
				$('table th input:checkbox').click(function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
				
				$.each($('.sel_per_page option'), function(index, item) {
					var cur_val = parseInt($.trim($(item).text()));
					if (cur_val == per_page)
						item.selected = true;
					else
						item.selected = false;
				});
				
			$('.sel_per_page').change(function(){
				$('#page_num_id').val($(this).val());
				$('#per_page_form').submit();
			});
		  });
		
		</script>
		

		<!-- ace scripts -->

		<script src="{{asset('js/ace/ace-elements.min.js')}}"></script>
		
		<!-- inline scripts related to this page -->
@endsection