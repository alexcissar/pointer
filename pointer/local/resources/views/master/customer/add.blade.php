@extends('app') @section('content')
<link rel="stylesheet" href="{{asset('css/master/customer.css')}}" />
<div class="page-content">
	<div class="page-header">
		<h1>運営者新規登録</h1>
	</div>
	<div class="row">
		
		<div class="modal-content">
			<div class="modal-header" id="insert_customer_form_header">
				<h4 class="blue bigger">下記の項目を入力し、『入力した内容で確認』をクリックして下さい。</h4>
				<h4 class="red smaller">※印のついてる箇所は入力必須項目です。</h4>
			</div>

			<div class="modal-body overflow-visible">
				<form class="form-horizontal" role="form" method="post" action="{{url('customer/insert')}}">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="name-field">運営者名</label>
						<div class="col-sm-9">
							<input type="text" class="col-xs-10 col-sm-8" id="name-field"
								name="name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="kananame-field">運営者カナ名</label>
						<div class="col-sm-9">
							<input type="text" class="col-xs-10 col-sm-8" id="kananame-field"
								name="kananame">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="email-field">ログインID</label>
						<div class="col-sm-9">
							<input type="text" class="col-xs-10 col-sm-6" id="email-field"
								name="email">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="password-field">パスワード</label>
						<div class="col-sm-9">
							<input type="password" class="col-xs-10 col-sm-7"
								id="password-field" name="password">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="confirm-field">パスワード確認</label>
						<div class="col-sm-9">
							<input type="password" class="col-xs-10 col-sm-7"
								id="confirm-field">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="confirmation-field">確認コード</label>
						<div class="col-sm-9">
							<textarea rows="5" cols="15" class="col-xs-10 col-sm-7"
								id="confirmation-field" name="confirmation"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-sm" data-dismiss="modal" id="cancel_btn">
							<i class="icon-remove"></i> 打ち消し
						</button>

						<button class="btn btn-sm btn-primary" type="submit" id="save_btn">
							<i class="icon-ok"></i> 保管
						</button>
					</div>
				</form>

			</div>
		</div>
		
	</div>
</div>

<script src="{{asset('js/ace/jquery-2.0.3.min.js')}}"></script>
<script src="{{asset('js/ace/bootstrap.min.js')}}"></script>

<!-- ace scripts -->

<script src="{{asset('js/ace/ace-elements.min.js')}}"></script>

<script src="{{asset('/js/ace/jquery.maskedinput.min.js')}}"></script>

<script src="{{asset('js/ace/fuelux/fuelux.spinner.min.js')}}"></script>
<!-- inline scripts related to this page -->

<script type="text/javascript">
	jQuery(function($) {

		$('input#name-field').focus();

		$('button#cancel_btn').click(function (e) {
			$('input#name-field').val('');
			$('input#kananame-field').val('');
			$('input#email-field').val('');
			$('input#password-field').val('');
			$('input#confirm-field').val('');
			$('input#confirmation-field').val('');
		});
		
	});
</script>


<!-- ace scripts -->

<script src="{{asset('js/ace/ace-elements.min.js')}}"></script>

<!-- inline scripts related to this page -->
@endsection
