@extends('app') @section('content')

<link rel="stylesheet" href="{{asset('css/themes/default/easyui.css')}}">
<link rel="stylesheet" href="{{asset('css/themes/icon.css')}}">
<link rel="stylesheet" href="{{asset('css/mapview/mapview.css')}}">

<link rel="stylesheet" href="{{asset('css/map_tiler/map_style.css')}}" />
<link rel="stylesheet" href="{{asset('css/pointer/pointer.css')}}" />
<link rel="stylesheet" href="{{asset('css/pointer/pointer_photo.css')}}" />
<link rel="stylesheet" href="{{asset('css/ace/colorbox.css')}}" />
<link rel="stylesheet" href="{{asset('pointer/uploadify.css')}}" />
<style type="text/css">
html,body {
	margin: 0;
	padding: 0;
	height: 100%;
	width: 100%;
}

body {
	width: 100%;
	height: 100%;
	background: #ffffff;
}
/*#map { position: fixed; height: 100%; width: 100%; background-color: #FFFFFF; }*/
.olImageLoadError {
	display: none !important;
}

div#search_box {
	background: transparent;
	width: 250px;
	height: 80px;
	border: 1px solid #FF0000;
	box-radius: 3 3 3 3px;
}

div.search_panel {
	font-weight: bold;
	width: 540px;
	height: 100px;
	border-style: double;
	border-radius: 20px;
	padding: 4px;
	display: inline-block;
	box-sizing: border-box;
	position: absolute;
	left: 50px;
	top: 3%;
	z-index: 10000;
	opacity: 0.8;
}
</style>

<!--<script src="{{asset('js/jquery.min.js')}}"></script>-->
<script src="{{asset('js/jquery.easyui.min.js')}}"></script>
<script src="{{asset('js/openlayer/OpenLayers.js')}}"></script>
<script src="{{asset('js/mapview/mapview.js')}}"></script>
<script src="{{asset('js/ace/jquery.colorbox-min.js')}}"></script>
<script type="text/javascript">
//rgb start
var photo_count=0;
	function refreshTrObject() {
		$('img.up_bt').bind('click',function (e) {
			pos = $(this).parents('tr').attr('pos');
			swap(pos, 1);		
		});

		$('img.down_bt').bind('click',function (e) {
			pos = $(this).parents('tr').attr('pos');
			swap(pos, 0);
		});

		$('.delete_row_bt').bind('click',function (e) {
			pos = $(this).parents('tr').attr('pos');
			current = $("tr[pos="+pos+"]");
			$(current).empty();
			$(current).remove();
		});
		
		/*$.each($('table#pointer_photo_table_id tbody tr[pos]'), function (index, item) {
			$(item).children('td:nth-child(4)').children('img.up_bt').bind('click',function (e) {
				pos = $(this).parents('tr').attr('pos');
				swap(pos, 1);		
			});

			$(item).children('td:nth-child(4)').children('img.down_bt').bind('click',function (e) {
				pos = $(this).parents('tr').attr('pos');
				swap(pos, 0);
			});

			$(item).children('td:nth-child(5)').children('.delete_row_bt').bind('click',function (e) {
				pos = $(this).parents('tr').attr('pos');
				current = $("tr[pos="+pos+"]");
				$(current).empty();
				$(current).remove();
			});
		});*/
	}
	function reorder(){
		  $.each($('#pointer_photo_table_id tr[pos]'), function(index, item) {
				$(item).children('.index_td').text(parseInt(index+1));
		  });
	  }
	function swap(pos,direction){
		  if (direction==0){//down
			  if (pos==(parseInt(photo_count)-1)){
				  return;
			  } 
			  next_pos = parseInt(pos) + 1;
		  }else{			//up
			  if (pos==0){
				  return;
			  }
			  next_pos = parseInt(pos) - 1;
		  }
		  current=$("tr[pos="+pos+"]");
		  target=$("tr[pos="+next_pos+"]");
		  
		  temp=current.html();
		  current.html(target.html());
		  target.html(temp);
		  reorder();
		  refreshTrObject();  
	  }
	//rgb end
	var map, layer;
	var mapBounds = new OpenLayers.Bounds(0.000000, -5575.000000, 7500.000000, 0.000000);
	var mapMinZoom = 0;
	var mapMaxZoom = 5;
	var mapMaxResolution = 1.000000;
	var gridBounds = new OpenLayers.Bounds(0.000000, -5575.000000, 7500.000000, 0.000000);

	var perPage = 0;
	var pageNum = 0;

	$(document).ready(function() {
		var options = {
    		controls: [],
    		maxExtent : gridBounds,
    		minResolution: mapMaxResolution,
    		numZoomLevels: mapMaxZoom+1
  		};
  		map = new OpenLayers.Map('map', options);
  		layer = new OpenLayers.Layer.XYZ( "MapTiler layer", "{{asset('image/map_tile')}}/${z}/${x}/${y}.png", {
    			transitionEffect: 'resize',
    			tileSize: new OpenLayers.Size(256, 256),
    			tileOrigin: new OpenLayers.LonLat(gridBounds.left, gridBounds.top)
  		});
  		
		//map.addLayer(searchLayer);
  		
	  	map.addLayer(layer);
	  	map.zoomToExtent(mapBounds);
	  	map.addControl(new OpenLayers.Control.Navigation());
	  	map.addControl(new OpenLayers.Control.PanZoom());
	  	map.addControl(new OpenLayers.Control.MousePosition({  numDigits: 0 }));
	  	map.addControl(new OpenLayers.Control.Permalink());
		
	  	function getPointerList() {
		  	
	  	}
	  	
	});

</script>

<div class="breadcrumbs" id="breadcrumbs">
	<script type="text/javascript">
		try { ace.settings.check('breadcrumbs', 'fixed'); } catch (e) { }
	</script>

	<ul class="breadcrumb">
		<li class="active">ポインタ管理</li>
	</ul>
	<!-- .breadcrumb -->
</div>

<div id="map-panel" style="height: 700px;">
	<div class="easyui-layout" style="width: 100%; height: 100%;">
		<div data-options="region:'north',border:false"
			style="height: 48px; background: #B3DFDA; padding: 10px">
			<div class="left-div">地図と地点</div>
			<div class="right-div">
				<label class="pull-right inline"> <small class="muted">地点の追加</small>

					<input id="gritter-light" checked="" type="checkbox"
					class="ace ace-switch ace-switch-5" /> <span class="lbl"></span>
				</label>
			</div>
			<div style="clear: both;"></div>
		</div>
		<div
			data-options="region:'east',split:true,collapsed:false,title:'East'"
			style="width: 350px; padding: 10px;">
			<form class="form-horizontal" role="form">
				<div class="space-4"></div>
				<div class="space-4"></div>
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="pointer_age_id">時代：</label>
					<div class="col-sm-7">
						<input type="text" id="pointer_age_id" class="col-xs-10 col-sm-5"
							placeholder="時代" />
					</div>
				</div>
				<div class="space-4"></div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="pointer_period_id">期間：</label>
					<div class="col-sm-7">
						<input type="text" id="pointer_period_id" class="col-xs-10 col-sm-5"
							placeholder="期間" />
					</div>
				</div>
				<div class="space-4"></div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right"
						for="pointer_category_id">カテゴリ</label>
					<div class="col-sm-7">
						<select class="col-xs-10 col-sm-5" id="pointer_category_id">
							<?php //echo count($category_data); exit(); ?>
							<?php foreach ($category_data as $category) { ?>
							<?php
								$category_id = $category ['id'];
								$category_name = $category ['category_name'];
								?>
							<option  value="<?php echo $category_id; ?>"><?php echo $category_name; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="space-4"></div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="pointer_name_id">ポインタ名：</label>
					<div class="col-sm-7">
						<input type="text" id="pointer_name_id" class="col-xs-10 col-sm-5"
							placeholder="ポインタ名" />
					</div>
				</div>
				<div class="space-4"></div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="pointer_kananame_id">ポインタカナ名：</label>
					<div class="col-sm-7">
						<input type="text" id="pointer_kananame_id" class="col-xs-10 col-sm-5"
							placeholder="ポインタカナ名" />
					</div>
				</div>
				<div class="space-4"></div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="pointer_position_latitude_id">座標：</label>
					<div class="col-sm-7">
					<input type="text" id="pointer_position_latitude_id" class="col-xs-10 col-sm-5"
							placeholder="latitude" />
					<input type="text" id="pointer_position_longitude_id" class="col-xs-10 col-sm-5"
							placeholder="longitude" />
					</div>
				</div>
				<div class="space-4"></div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="pointer_address_id">現在の住所：</label>
					<div class="col-sm-7">
					<input type="text" id="pointer_address_id" class="col-xs-10 col-sm-5"
							placeholder="current_address" />
					</div>
				</div>
				<div class="space-4"></div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right"
						for="pointer_ids_id">pointers</label>
					<div class="col-sm-7">
						<select class="col-xs-10 col-sm-5" id="pointer_ids_id">
							<option value="-1">please select</option>
							<?php foreach ($pointers as $value) { ?>
							<option  value="<?php echo $value->id; ?>"><?php echo $value->pointer_name; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="space-4"></div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="pointer_content_id">内容：</label>
					<div class="col-sm-7">
						<textarea placeholder="content" id="pointer_content_id" name="content"
							class=""></textarea>
					</div>
				</div>
				<div class="space-4"></div>


				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="pointer_extra_id">備考：</label>
					<div class="col-sm-7">
						<textarea placeholder="extra" id="pointer_extra_id"
							name="etc_content" class=""></textarea>
					</div>
				</div>
				<div class="space-4"></div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="pointer_images">関連の写真：</label>
					<div class="col-sm-7" >
					<div id="pointer_images">
						<ul>
						<?php foreach ($image_data as $key=>$val){?>
						<li>
						<img src="{{asset('image/pointer/uploaded_photos/'.$val['photo_name'])}}" 
						alt="<?php echo $val['photo_name']?>"/>	 
						</li>
						<?php }?>
						</ul>
					</div>
					<a data-toggle="modal" id="pointer_photo_link_id" class="btn btn-primary blue" role="button" href="#image_manage">change photos</a>
					
					</div>
				</div>
				<div class="space-4"></div>
	
			   <div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="pointer_display_status_id">display</label>
					<div class="col-sm-7" id="pointer_display_status_id">

						<input type="radio" value="show" class="pointer_show_status" name="pointer_show_name" id="pointer_show_id" /><span>show</span>
						<input type="radio" value="none" class="pointer_show_status" name="pointer_show_name" id="pointer_none_id" /><span>none</span>

					</div>
				</div>
				<div class="space-4"></div>
				<div class="form-group">
					<button class="btn btn-success" id="pointer_register_id">register</button>
					<button class="btn btn-danger" id="pointer_delete_id">delete</button>
				</div>
				<div class="space-4"></div>
			
			</form>
		</div>

		<div
			data-options="region:'south',border:true,split:true,collapsed:true,title:'South'"
			style="height: 200px; background: #A9FACD; padding: 10px;">

			<div class="row">
				<div class="col-xs-12">
					<div class="table-responsive">
						<div id="sample-table-2_wrapper" class="dataTables_wrapper" role="grid">
							<div class="row">
								<div class="col-sm-4">
									<div>
										<label>フィルター
											<input type="text">
										</label>
									</div>
								</div>
								<div class="col-sm-8">
									
								</div>
								<table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
									<thead>
										<tr role="row">
											<th class="center">No</th>
											<th class="center">カテゴリ</th>
											<th class="center">ポインタ名</th>
											<th class="center">現在の住所</th>
										</tr>
									</thead>
									<tbody id="search_result">
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div data-options="region:'center'">
			<div class="map_view_div" id="map">
				<div class="search_panel">

					<div class="space-4"></div>
					<div style="width: 540px;">
						<label>時代：</label> <input type="radio" id="edo_age"> <label
							for="edo_age">江戸</label> <input type="radio" id="policy_age"> <label
							for="policy_age">明治</label> <label style="margin-left: 60px;">カテゴリ：</label>
						<select style="width: 100px; height: 20px;" id="category_search">
								<option value="0">please select...</option>
							<?php foreach ($category_data as $category) {?>
								<option value="<?php echo $category['id']?>"><?php echo $category['category_name']?></option>
							<?php }?>
						</select>
						<button class="btn btn-xs" style="margin-left: 80px;"
							id="search_btn">検索</button>
					</div>
					<div class="space-4"></div>
					<div style="width: 540px;">
						<label for="pointer_search">ポインタ名：</label> <input type="text"
							style="width: 100px;" id="pointer_search" name="pointer_search">
						<label style="margin-left: 5px;">テキスト：</label> <input type="text"
							style="width: 100px;" id="text_search" name="text_search">
						<button class="btn btn-xs" style="margin-left: 80px;"
							id="clear_search_btn">クリア</button>
					</div>

				</div>
			</div>

		</div>
	</div>
</div>

<div id="image_manage" class="modal" tabindex="-1">

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Image Manager</h4>
			</div>
	
			<div class="modal-body overflow-visible">
				<div class="row">
	
					<div class="col-xs-12 col-sm-12">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="pointer_photo_table_id" class="col-xs-12 col-sm-12">
								<thead>
									<tr>
										<th class="center">
											No
										</th>
										<th>Image</th>
										<th>Comment</th>
										<th>position</th>				
										<th></th>
									</tr>
								</thead>
	
								<tbody>
								   <?php 
								   $index=0;
								   foreach ($image_data as $key=>$val){
									$index++;
								   ?>
									<tr pos="<?php echo $index-1;?>">
										<td class="col-sm-1 index_td">
											<?php echo $index;?>
										</td>
	
										<td class="col-sm-1 image_td">
											<ul class="ace-thumbnails">
												<li>
													<a data-rel="colorbox" href="assets/images/gallery/image-3.jpg" class="cboxElement">
														<img src="{{asset('image/pointer/uploaded_photos/'.$val['photo_name'])}}" 
														class="col-xs-12 pointer_image_class" alt="<?php echo $val['photo_name']?>"/>
													</a>
		
													<div class="tools tools-bottom">
														<a href="#">
															<i class="icon-pencil"></i>
														</a>
														<a href="#">
															<i class="icon-remove red"></i>
														</a>
													</div>
												</li>
											</ul>
										</td>
										<td class="col-sm-10">
											<textarea class="photo_comment  col-xs-12" ><?php echo $val['photo_comment']?></textarea>
										</td>
										<td  class="col-sm-1">
											<img alt="up" src="{{asset('image/map_tile/up.png')}}" class="up_bt" >
											<img alt="down" src="{{asset('image/map_tile/down.png')}}" class="down_bt"></td>
										<td class="col-sm-1">
											<button data-toggle="button" class="btn btn-sm btn-danger delete_row_bt" type="button">Delete</button>
										</td>
									</tr>
									<?php }?>
									<tr id="new_image_tr">
										<td class="">*</td>
										<td class="">		
										<div id="new_image_upload_div">
										</div>
										</td>
										<td class="">
											
										</td>
										<td class=""></td>
										<td class="insert_new_row_bt">
											
										</td>

									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
	
				<div class="modal-footer">
					<button class="btn btn-sm" data-dismiss="modal">
						<i class="icon-remove"></i>
						打ち消し
					</button>
					<button type="button" class="btn btn-sm btn-primary create_new_pointer_data_btn" data-dismiss="modal">
						<i class="icon-ok"></i>
						保管
					</button>
				</div>
			</div>
		</div>
	</div><!-- PAGE CONTENT ENDS -->
		<input type="hidden" id="pointer_id_value" value="<?php echo $server_pointer_id?>" />	
		<input type="hidden" id="pointer_photo_count_value" value="<?php echo count($image_data)?>" />	
		<input type="hidden" id="new_uploaded_file_name_input" value=""/>
<script type="text/javascript">
								
	$(document).ready(function() {
	
		var age = 3;
		
		$("input#policy_age").attr("checked",false);
		$("input#edo_age").attr("checked",false);
		$("input#pointer_search").val("");
		$("input#text_search").val("");
		
			
		$("button#clear_search_btn").click(function() {
			$("input#pointer_search").val("");
			$("input#text_search").val("");
		});

		$("input#policy_age").click(function() {
			$(this).attr("checked",true);
			age = 2;
			$("input#edo_age").attr("checked",false);
		});

		$("input#edo_age").click(function() {
			$(this).attr("checked",true);
			age = 1;
			$("input#policy_age").attr("checked",false);
		});


		$.ajax({
			headers: {
	              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        },
	        type:"post",
	        url:"{{url('pointer/pointers')}}",
	        dataType:"json",
	        data:{'pointers':'giveme'},
	        success:function(data) {
	        	var html = "";
	        	$('table#sample-table-2 tbody#search_result').html(html);
	        	var data_length = data.length; 
				for (var i=0; i<data_length; i++) {
					var each_data = data[i];

					var search_category_name = each_data['category_name'];
					var search_pointer_name = each_data['pointer_name'];
					var search_pointer_curaddress = each_data['pointer_curaddress'];

					var no = i + 1;
					html += "<tr>";
					html += "<td class='center'>"+no+"</td>";
					html += "<td class='center'>"+search_category_name+"</td>";
					html += "<td class='center'>"+search_pointer_name+"</td>";
					html += "<td class='center'>"+search_pointer_curaddress+"</td>";
					html += "</tr>";
				}

				$('table#sample-table-2 tbody#search_result').html(html);
	        }
		});
		
		$("button#search_btn").bind('click', function() {
			var pointer_name = $("input#pointer_search").val();
			var text = $("input#text_search").val();
			var category = $("select#category_search").val();
			
			var html = "";
			$('table#sample-table-2 tbody#search_result').html(html);
			$.ajax({
				headers: {
		              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        },
				type:'post',
				dataType:'json',
				data:{'pointer_name':pointer_name,'text':text,'age':age,'category':category},
				url:"{{url('pointer/pt_search')}}",
				success:function(data) {
					
					var data_length = data.length; 
					for (var i=0; i<data_length; i++) {
						var each_data = data[i];

						var search_category_name = each_data['category_name'];
						var search_pointer_name = each_data['pointer_name'];
						var search_pointer_curaddress = each_data['pointer_curaddress'];

						var no = i + 1;
						html += "<tr>";
						html += "<td class='center'>"+no+"</td>";
						html += "<td class='center'>"+search_category_name+"</td>";
						html += "<td class='center'>"+search_pointer_name+"</td>";
						html += "<td class='center'>"+search_pointer_curaddress+"</td>";
						html += "</tr>";
					}

					$('table#sample-table-2 tbody#search_result').html(html);
				}
			});
			
		});

		//rgb
		var photo=new Array();
		var photo_content= new Array();
		$("#pointer_register_id").click(function(e){
			var mode="";
			var pointer_id="";
			if($("#gritter-light").hasClass('updating_on')==true){
				mode="update";
				pointer_id=$("#pointer_id_value").val();
			}else{
				
				mode="create";
			}
 			var pointer_age=$("#pointer_age_id").val();
 			var pointer_period=$("#pointer_period_id").val();
			var pointer_category=$("#pointer_category_id").val();
			var pointer_name=$("#pointer_name_id").val();
			var pointer_kananame=$("#pointer_kananame_id").val();
			var pointer_position_latitude=$("#pointer_position_latitude_id").val();
			var pointer_position_longitude=$("#pointer_position_longitude_id").val();
			var pointer_address=$("#pointer_address_id").val();
			var pointer_content=$("#pointer_content_id").val();
			var pointer_extra=$("#pointer_extra_id").val();
			var pointer_ids_id=$("#pointer_ids_id").val();
			var display = $('[name="pointer_show_name"]:checked').val();
			$.ajax({
			  	headers: {
		              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        },
		        type:"post",
				url: "{{ url('pointer/save_pointer_image') }}",
          		dataType: 'json',
				data: { 'photo' : photo,
					'content':photo_content,
					'mode':mode,
					'pointer_id':pointer_id,
					'pointer_age':pointer_age,
					'pointer_period':pointer_period,
					'pointer_category':pointer_category,
					'pointer_name':pointer_name,
					'pointer_kananame':pointer_kananame,
					'pointer_position_latitude':pointer_position_latitude,
					'pointer_position_longitude':pointer_position_longitude,
					'pointer_address':pointer_address,
					'pointer_group':pointer_ids_id,
					'pointer_content':pointer_content,
					'pointer_extra':pointer_extra,
					'pointer_display':display
					},
				success: function(json) {			
					
				}
					
		   });
			e.preventDefault();
		});
		$("#gritter-light").click(function(){
			$(this).toggleClass("updating_on");
		});
		$('[name="pointer_show_name"]:first').attr('checked',true);
		photo_count=$("#pointer_photo_count_value").val();
		$("#pointer_category_id").children('option:first').attr('selected','');
		refreshTrObject();
		$(".create_new_pointer_data_btn").click(function(){
			init_variable();
		});
		init_variable();
	    function init_variable(){
	    	photo=null;
			photo_content=null;
			photo=new Array();
			photo_content=new Array();
			var gallery_content="";
			$.each($('#pointer_photo_table_id tr[pos]'), function(index, item) {
					var content_temp=$(item).find('textarea').val();
                photo_content.push(content_temp);
				var photo_temp=$(item).find('td:nth-child(2) ul li a img ').attr('alt');
				var photo_url=$(item).find('td:nth-child(2) ul li a img ').attr('src');
				gallery_content=gallery_content+"<li><img alt='"+photo_temp+"' src='"+photo_url+"'></li>";
				photo.push(photo_temp);
		    });
			$("#pointer_images  ul").empty();
		    $("#pointer_images  ul").append(gallery_content);
	    }
	  
	  $('div#new_image_upload_div').uploadify({
		  
	  });
	  //rgb//
	});
	
</script>
<style>
/*html, body { margin:0; padding: 0; height: 100%; width: 100%; }*/
/*body { width:100%; height:100%; background: #ffffff; }*/
/*.olImageLoadError {  display: none !important; }*/
</style>

@endsection



