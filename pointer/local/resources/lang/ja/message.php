<?php

return [
    'pointer_delete_confirm' => '選択した項目を削除します。よろしいですか？',
    'pointer_delete_confirm_validation' => '削除しようとするグループ設定を入力してください。!',
];
