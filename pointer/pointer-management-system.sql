-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2015 at 01:26 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pointer-management-system`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_user`
--

DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE IF NOT EXISTS `admin_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Admin User ID',
  `name` varchar(40) DEFAULT NULL COMMENT 'User Name',
  `kananame` varchar(40) DEFAULT NULL COMMENT 'User Kana Name',
  `email` varchar(128) DEFAULT NULL COMMENT 'User Email',
  `password` varchar(255) DEFAULT NULL COMMENT 'User Password',
  `confirm_code` varchar(255) DEFAULT NULL COMMENT 'Confirm Code',
  `logdate` timestamp NULL DEFAULT NULL COMMENT 'User Last Login Time',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'User Created Time',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'User Modified Time',
  `remember_token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Admin User Table' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin_user`
--

INSERT INTO `admin_user` (`id`, `name`, `kananame`, `email`, `password`, `confirm_code`, `logdate`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, '管理者', 'カンリシャ', 'admin@test.com', '$2y$10$utS7KZMvVgW480kvRPF20.LNl9OvfW9klxcW8AHa23r9y9/qKiEXG', 'q', '2015-07-13 08:12:28', '0000-00-00 00:00:00', '2015-07-13 08:12:28', 'VBDfS3yTmxwM6zdJV0hkmkHeJVSlfz6X61htz2G3Uu5dDADN4b8qWJfRJbYd');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Category ID',
  `category_name` varchar(40) DEFAULT NULL COMMENT 'Category Name',
  `pointer_color` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Category Created Time',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Category Updated Time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Category Table' AUTO_INCREMENT=11 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `pointer_color`, `created_at`, `updated_at`) VALUES
(1, 'category1', '#7bd148', '2015-05-27 01:51:41', '2015-05-27 02:34:27'),
(2, 'category2', '#9fe1e7', '2015-05-27 02:34:48', '2015-05-27 02:34:48'),
(3, 'category3', '#b99aff', '2015-05-27 04:07:33', '2015-05-27 18:34:05'),
(6, 'category_edit', '#f691b2', '2015-06-17 07:11:36', '2015-07-03 09:26:40'),
(8, 'ww', '#42d692', '2015-07-03 06:55:41', '2015-07-03 09:41:43'),
(9, 'aaa', '#9fe1e7', '2015-07-06 07:58:10', '2015-07-06 07:58:10'),
(10, 'dasfd', '#ffad46', '2015-07-09 03:36:15', '2015-07-09 03:36:15');

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
CREATE TABLE IF NOT EXISTS `group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Group ID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Group Created Time',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Group Updated Time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Group Table' AUTO_INCREMENT=40 ;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`id`, `created_at`, `updated_at`) VALUES
(1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '2015-06-07 16:01:58', '2015-06-07 16:01:58'),
(3, '2015-06-07 16:03:35', '2015-06-07 16:03:35'),
(4, '2015-06-07 16:03:46', '2015-06-07 16:03:46'),
(5, '2015-06-11 02:08:05', '2015-06-11 02:08:05'),
(6, '2015-06-11 02:31:08', '2015-06-11 02:31:08'),
(7, '2015-06-11 02:56:51', '2015-06-11 02:56:51'),
(8, '2015-06-12 01:41:06', '2015-06-12 01:41:06'),
(9, '2015-06-17 11:00:45', '2015-06-17 11:00:45'),
(10, '2015-06-17 11:14:48', '2015-06-17 11:14:48'),
(11, '2015-06-17 11:16:10', '2015-06-17 11:16:10'),
(12, '2015-06-17 11:16:55', '2015-06-17 11:16:55'),
(13, '2015-06-17 11:18:04', '2015-06-17 11:18:04'),
(14, '2015-06-17 11:18:46', '2015-06-17 11:18:46'),
(15, '2015-06-17 11:21:16', '2015-06-17 11:21:16'),
(16, '2015-06-18 18:34:51', '2015-06-18 18:34:51'),
(17, '2015-06-26 01:23:46', '2015-06-26 01:23:46'),
(18, '2015-06-26 01:51:48', '2015-06-26 01:51:48'),
(19, '2015-06-26 01:54:14', '2015-06-26 01:54:14'),
(20, '2015-06-26 01:59:11', '2015-06-26 01:59:11'),
(21, '2015-06-26 02:02:08', '2015-06-26 02:02:08'),
(22, '2015-06-26 02:04:05', '2015-06-26 02:04:05'),
(23, '2015-06-26 02:06:54', '2015-06-26 02:06:54'),
(24, '2015-06-26 02:08:06', '2015-06-26 02:08:06'),
(25, '2015-06-26 02:11:51', '2015-06-26 02:11:51'),
(26, '2015-06-26 02:29:39', '2015-06-26 02:29:39'),
(27, '2015-06-26 02:57:02', '2015-06-26 02:57:02'),
(28, '2015-06-26 03:00:43', '2015-06-26 03:00:43'),
(29, '2015-06-26 03:08:04', '2015-06-26 03:08:04'),
(30, '2015-06-26 03:09:27', '2015-06-26 03:09:27'),
(31, '2015-06-26 03:13:26', '2015-06-26 03:13:26'),
(32, '2015-06-26 03:22:53', '2015-06-26 03:22:53'),
(33, '2015-06-26 03:24:46', '2015-06-26 03:24:46'),
(34, '2015-06-26 03:26:50', '2015-06-26 03:26:50'),
(35, '2015-06-26 03:29:58', '2015-06-26 03:29:58'),
(36, '2015-06-26 03:30:55', '2015-06-26 03:30:55'),
(37, '2015-06-26 03:40:50', '2015-06-26 03:40:50'),
(38, '2015-06-26 08:25:12', '2015-06-26 08:25:12'),
(39, '2015-06-26 11:29:01', '2015-06-26 11:29:01');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(128) DEFAULT NULL COMMENT 'Admin User Email',
  `token` varchar(255) DEFAULT NULL COMMENT 'Token',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Password Reset Time'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Password Resets Table';

-- --------------------------------------------------------

--
-- Table structure for table `pointer`
--

DROP TABLE IF EXISTS `pointer`;
CREATE TABLE IF NOT EXISTS `pointer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Pointer ID',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Group ID',
  `pointer_age` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Pointer Age',
  `pointer_period` varchar(128) DEFAULT NULL COMMENT 'Category ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `pointer_name` varchar(128) DEFAULT NULL COMMENT 'Pointer Name',
  `pointer_kananame` varchar(128) DEFAULT NULL COMMENT 'Pointer Kana Name',
  `pointer_address` varchar(255) DEFAULT NULL COMMENT 'Pointer Address',
  `pointer_longitude` double DEFAULT NULL COMMENT 'Pointer Position(Logitude)',
  `pointer_latitude` double DEFAULT NULL COMMENT 'Pointer Position(Latitude)',
  `pointer_longlatdisp` varchar(128) DEFAULT '0',
  `pointer_curaddress` varchar(255) DEFAULT NULL COMMENT 'Pointer Current Address',
  `pointer_comment` text COMMENT 'Pointer Comment',
  `pointer_extra` text COMMENT 'Pointer Extra',
  `pointer_show` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag if pointer show',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Pointer Created Time',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Pointer Modified Time',
  PRIMARY KEY (`id`),
  KEY `FK_pointer` (`group_id`),
  KEY `FK_pointer2` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Pointer Table' AUTO_INCREMENT=86 ;

--
-- Dumping data for table `pointer`
--

INSERT INTO `pointer` (`id`, `group_id`, `pointer_age`, `pointer_period`, `category_id`, `pointer_name`, `pointer_kananame`, `pointer_address`, `pointer_longitude`, `pointer_latitude`, `pointer_longlatdisp`, `pointer_curaddress`, `pointer_comment`, `pointer_extra`, `pointer_show`, `created_at`, `updated_at`) VALUES
(53, 8, 3, 'pos', 2, 'icon', 'test', 'aaa', 2191602.4749925807, 2935181.886150766, '2191602.4749925807:2935181.886150766', 'aaa', '', '', 1, '2015-07-03 09:57:53', '2015-06-26 11:26:23'),
(55, 10, 2, 'kjh', 3, 'qqqqqqqq', 'qqqqqqqq', 'q', 4816, -3584, '139° 45’ 21.87” (E)    35° 41’ 0.08” (S)', 'q', '', '', 1, '2015-07-03 09:58:01', '2015-06-26 11:26:17'),
(56, 11, 3, 'h', 8, 'tgfr', 'tgfr', 'a', -12445171.197279252, -3796168.572754994, '-12445171.197279252:-3796168.572754994', 'a', '', '', 1, '2015-07-03 09:58:17', '2015-06-26 11:27:04'),
(58, 13, 2, 'h', 6, 'tgfr23', 'tgfr', 'a', 4560, -3072, '139° 45’ 19.04” (E)    35° 41’ 3.76” (S)', 'a', '', '', 1, '2015-07-03 09:58:08', '2015-06-26 11:27:00'),
(59, 14, 3, 'h', 3, 'tgfr234', 'tgfr', 'a', 6105178.323193602, 2935181.886150766, '6105178.323193602:2935181.886150766', 'a', '', '', 1, '2015-06-26 10:26:56', '2015-06-26 11:26:56'),
(60, 15, 1, 'asfd', 6, 'ghost', 'k', 'a', 3522218.2633809224, 8570731.10756024, '3522218.2633809224:8570731.10756024', 'a', '', '', 1, '2015-07-03 09:58:11', '2015-06-26 08:23:53'),
(61, 16, 1, 'fdsa', 1, 'ggg', 'gggg', 'a', 5400734.670517415, -1448023.0638343804, '5400734.670517415:-1448023.0638343804', 'a', '', '', 1, '2015-06-26 10:26:28', '2015-06-26 11:26:28'),
(62, 17, 1, 'newpointer', 8, 'asf', 'asdfasdfasdf', 'aaaaaaa', -6809621.975869782, 10762333.582552813, '-6809621.975869782:10762333.582552813', 'aaaaaaa', '', '', 0, '2015-07-03 09:58:14', '2015-06-26 01:23:46'),
(63, 18, 1, 'afds', 2, 'afd', 'afds', 'fdsa', -7592337.145509981, 9196903.243272405, '-7592337.145509981:9196903.243272405', 'fdsa', '', '', 0, '2015-07-03 09:57:48', '2015-06-26 01:51:48'),
(64, 19, 1, 'asdfas', 1, 'dfasdf', 'asdfasdfasdf', 'aaa', -5870363.772301532, 4422340.708467156, '-5870363.772301532:4422340.708467156', 'aaa', '', '', 0, '2015-06-26 01:54:14', '2015-06-26 01:54:14'),
(65, 20, 1, 'fffffff', 3, 'delete example', 'akana', 'aa', -6339992.874085657, 5987771.047747564, '-6339992.874085657:5987771.047747564', 'aa', '', '', 0, '2015-07-03 09:57:59', '2015-06-26 01:59:11'),
(71, 26, 1, 'a', 8, 'd', 'a', 'f', -26612315.767766964, 13227886.366919464, '-26612315.767766964:13227886.366919464', 'f', '', '', 1, '2015-07-13 08:30:42', '2015-07-13 08:30:42'),
(72, 27, 1, 'asdfasdf', 3, 'asdfasdfasdfasf', 'asdf', 'asdfas', -8766409.899970293, 8022830.488812096, '-8766409.899970293:8022830.488812096', 'asdfas', 'fa', '', 0, '2015-07-03 09:57:59', '2015-06-26 02:57:02'),
(73, 28, 1, 'adsfas', 2, 'fdasdaaaa', 'fasdfasdfasd', 'a', 5713820.738373496, 7083572.28524385, '5713820.738373496:7083572.28524385', 'a', '', '', 0, '2015-07-03 09:57:49', '2015-06-26 03:00:43'),
(74, 29, 1, 'asdfasfd', 1, 'asdfaaaa', 'aa', 'f', -26427671.235505696, 11539215.79817485, '-26427671.235505696:11539215.79817485', 'f', '', '', 1, '2015-07-13 08:32:58', '2015-07-13 08:32:58'),
(75, 30, 1, 'qrew', 8, 'qrewqwre', 'rewqrewqrew', 'q', -10488383.273178741, 3991847.3651650455, '-10488383.273178741:3991847.3651650455', 'q', '', '', 0, '2015-07-03 09:58:15', '2015-06-26 03:09:27'),
(76, 31, 1, 'asdf', 6, 'aaaaaaaaaaafioio', 'a', 'a', -1565430.3392804, 2582960.0598127, '-1565430.3392804042:2582960.0598126743', 'ex_a', '', '', 1, '2015-07-08 00:27:49', '2015-07-08 01:27:49'),
(77, 32, 1, 'asdf', 6, 'aaaaaf', 'asfasf', 'f', 6757986.101725392, 2749319.12840252, '6757986.101725392:2749319.12840252', 'f', '', '', 1, '2015-07-13 08:30:18', '2015-07-13 08:30:18'),
(78, 33, 1, 'rew', 1, 'new', 'new', 's', -3905902.1700672917, 547900.6187481433, '-3905902.1700672917:547900.6187481433', 's', '', '', 0, '2015-06-26 03:24:46', '2015-06-26 03:24:46'),
(79, 34, 1, 'sfgsdgf', 8, 'sdfgsdgf', 'sgfsdgf', 's', -3600489.7803449407, 2817774.610704735, '-3600489.7803449407:2817774.610704735', 's', '', '', 0, '2015-07-03 09:58:15', '2015-06-26 03:26:50'),
(80, 35, 1, 'afsdf', 6, 'asdfah', 'sfd', 'f', -469629.10178412497, 8922952.933898337, '-469629.10178412497:8922952.933898337', 'f', '', '', 0, '2015-07-13 08:54:54', '2015-07-13 08:54:53'),
(81, 36, 1, 'fff', 3, 'aaa', 'vvv', 'a', 7514065.628546, 821850.92812221, '7514065.62854597:821850.928122215', 'ex_a', '', '', 1, '2015-07-03 10:22:22', '2015-07-03 11:22:22'),
(82, 37, 1, 'afds', 2, 'asdfasp', 'dfasdf', 'a', -9079495.967826374, 7866287.454884056, '-9079495.967826374:7866287.454884056', 'a', '', '', 1, '2015-07-03 09:57:52', '2015-06-26 03:40:50'),
(83, 38, 1, 'REDSTAR', 3, 'OK', 'OKKANA', 'TOKYO', 788379.4279218044, 7431126.832230848, '788379.4279218044:7431126.832230848', 'TOKYO', '', '', 1, '2015-06-26 08:25:12', '2015-06-26 08:25:12'),
(84, 39, 1, 'ff', 3, 'jia', 'jia', 'a', -5372390.935160016, 6966059.235847612, '-5372390.935160016:6966059.235847612', 'a', '', '', 1, '2015-07-03 09:58:02', '2015-06-26 11:29:01'),
(85, 10, 1, 'xin shi dai', 2, 'new pointer', 'new poiner kana name', 'Kwang Bong''s House', 5870484.312426439, 7286300.610962336, '5870484.312426439:7286300.610962336', 'Kwang Bong''s House', 'wow! Great!', 'Other Content!', 1, '2015-06-27 03:12:43', '2015-06-27 04:12:43');

-- --------------------------------------------------------

--
-- Table structure for table `pointer_photo`
--

DROP TABLE IF EXISTS `pointer_photo`;
CREATE TABLE IF NOT EXISTS `pointer_photo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Pointer Photo ID',
  `pointer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Pointer ID for This Photo',
  `photo_name` varchar(255) DEFAULT NULL COMMENT 'Photo Name',
  `photo_comment` text COMMENT 'Photo Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Photo Created Time',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Photo Modified Time',
  PRIMARY KEY (`id`),
  KEY `FK_pointer_photo` (`pointer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Pointer Photo Table' AUTO_INCREMENT=294 ;

--
-- Dumping data for table `pointer_photo`
--

INSERT INTO `pointer_photo` (`id`, `pointer_id`, `photo_name`, `photo_comment`, `created_at`, `updated_at`) VALUES
(123, 62, 'god15-06-11-13-31-05Blue hills.jpg', 'aa', '2015-06-26 01:23:46', '2015-06-26 01:23:46'),
(124, 62, 'god15-06-12-06-17-40Winter.jpg', 'bb', '2015-06-26 01:23:46', '2015-06-26 01:23:46'),
(125, 62, 'god15-06-12-08-37-32Sunset.jpg', 'cc', '2015-06-26 01:23:46', '2015-06-26 01:23:46'),
(126, 63, 'god15-06-11-13-31-05Blue hills.jpg', 'aa', '2015-06-26 01:51:48', '2015-06-26 01:51:48'),
(127, 63, 'god15-06-12-06-17-40Winter.jpg', 'bb', '2015-06-26 01:51:48', '2015-06-26 01:51:48'),
(128, 63, 'god15-06-12-08-37-32Sunset.jpg', 'cc', '2015-06-26 01:51:48', '2015-06-26 01:51:48'),
(129, 64, 'god15-06-11-13-31-05Blue hills.jpg', 'aa', '2015-06-26 01:54:14', '2015-06-26 01:54:14'),
(130, 64, 'god15-06-12-06-17-40Winter.jpg', 'bb', '2015-06-26 01:54:14', '2015-06-26 01:54:14'),
(131, 64, 'god15-06-12-08-37-32Sunset.jpg', 'cc', '2015-06-26 01:54:14', '2015-06-26 01:54:14'),
(132, 65, 'god15-06-11-13-31-05Blue hills.jpg', 'aa', '2015-06-26 01:59:11', '2015-06-26 01:59:11'),
(133, 65, 'god15-06-12-06-17-40Winter.jpg', 'bb', '2015-06-26 01:59:11', '2015-06-26 01:59:11'),
(134, 65, 'god15-06-12-08-37-32Sunset.jpg', 'cc', '2015-06-26 01:59:11', '2015-06-26 01:59:11'),
(153, 72, 'god15-06-11-13-31-05Blue hills.jpg', 'aa', '2015-06-26 02:57:02', '2015-06-26 02:57:02'),
(154, 72, 'god15-06-12-06-17-40Winter.jpg', 'bb', '2015-06-26 02:57:02', '2015-06-26 02:57:02'),
(155, 72, 'god15-06-12-08-37-32Sunset.jpg', 'cc', '2015-06-26 02:57:02', '2015-06-26 02:57:02'),
(156, 73, 'god15-06-11-13-31-05Blue hills.jpg', 'aa', '2015-06-26 03:00:43', '2015-06-26 03:00:43'),
(157, 73, 'god15-06-12-06-17-40Winter.jpg', 'bb', '2015-06-26 03:00:43', '2015-06-26 03:00:43'),
(158, 73, 'god15-06-12-08-37-32Sunset.jpg', 'cc', '2015-06-26 03:00:43', '2015-06-26 03:00:43'),
(162, 75, 'god15-06-11-13-31-05Blue hills.jpg', 'aa', '2015-06-26 03:09:27', '2015-06-26 03:09:27'),
(163, 75, 'god15-06-12-06-17-40Winter.jpg', 'bb', '2015-06-26 03:09:27', '2015-06-26 03:09:27'),
(164, 75, 'god15-06-12-08-37-32Sunset.jpg', 'cc', '2015-06-26 03:09:27', '2015-06-26 03:09:27'),
(171, 78, 'god15-06-11-13-31-05Blue hills.jpg', 'aa', '2015-06-26 03:24:46', '2015-06-26 03:24:46'),
(172, 78, 'god15-06-12-06-17-40Winter.jpg', 'bb', '2015-06-26 03:24:46', '2015-06-26 03:24:46'),
(173, 78, 'god15-06-12-08-37-32Sunset.jpg', 'cc', '2015-06-26 03:24:46', '2015-06-26 03:24:46'),
(174, 79, 'god15-06-11-13-31-05Blue hills.jpg', 'aa', '2015-06-26 03:26:50', '2015-06-26 03:26:50'),
(175, 79, 'god15-06-12-06-17-40Winter.jpg', 'bb', '2015-06-26 03:26:50', '2015-06-26 03:26:50'),
(176, 79, 'god15-06-12-08-37-32Sunset.jpg', 'cc', '2015-06-26 03:26:50', '2015-06-26 03:26:50'),
(216, 60, 'god15-06-11-13-31-05Blue hills.jpg', '', '2015-06-26 08:23:53', '2015-06-26 08:23:53'),
(217, 60, 'god15-06-12-06-17-40Winter.jpg', '', '2015-06-26 08:23:53', '2015-06-26 08:23:53'),
(218, 60, 'god15-06-12-08-37-32Sunset.jpg', '', '2015-06-26 08:23:53', '2015-06-26 08:23:53'),
(249, 55, 'god15-06-11-13-31-05Blue hills.jpg', '', '2015-06-26 11:26:17', '2015-06-26 11:26:17'),
(250, 55, 'god15-06-12-06-17-40Winter.jpg', '', '2015-06-26 11:26:17', '2015-06-26 11:26:17'),
(251, 55, 'god15-06-12-08-37-32Sunset.jpg', '', '2015-06-26 11:26:17', '2015-06-26 11:26:17'),
(252, 53, 'god15-06-11-13-31-05Blue hills.jpg', '', '2015-06-26 11:26:23', '2015-06-26 11:26:23'),
(253, 53, 'god15-06-12-06-17-40Winter.jpg', '', '2015-06-26 11:26:23', '2015-06-26 11:26:23'),
(254, 53, 'god15-06-12-08-37-32Sunset.jpg', '', '2015-06-26 11:26:23', '2015-06-26 11:26:23'),
(255, 61, 'god15-06-11-13-31-05Blue hills.jpg', '', '2015-06-26 11:26:28', '2015-06-26 11:26:28'),
(256, 61, 'god15-06-12-06-17-40Winter.jpg', '', '2015-06-26 11:26:28', '2015-06-26 11:26:28'),
(257, 61, 'god15-06-12-08-37-32Sunset.jpg', '', '2015-06-26 11:26:28', '2015-06-26 11:26:28'),
(258, 59, 'god15-06-11-13-31-05Blue hills.jpg', '', '2015-06-26 11:26:56', '2015-06-26 11:26:56'),
(259, 59, 'god15-06-12-06-17-40Winter.jpg', '', '2015-06-26 11:26:56', '2015-06-26 11:26:56'),
(260, 59, 'god15-06-12-08-37-32Sunset.jpg', '', '2015-06-26 11:26:56', '2015-06-26 11:26:56'),
(261, 58, 'god15-06-11-13-31-05Blue hills.jpg', '', '2015-06-26 11:27:00', '2015-06-26 11:27:00'),
(262, 58, 'god15-06-12-06-17-40Winter.jpg', '', '2015-06-26 11:27:00', '2015-06-26 11:27:00'),
(263, 58, 'god15-06-12-08-37-32Sunset.jpg', '', '2015-06-26 11:27:00', '2015-06-26 11:27:00'),
(264, 56, 'god15-06-11-13-31-05Blue hills.jpg', '', '2015-06-26 11:27:04', '2015-06-26 11:27:04'),
(265, 56, 'god15-06-12-06-17-40Winter.jpg', '', '2015-06-26 11:27:04', '2015-06-26 11:27:04'),
(266, 56, 'god15-06-12-08-37-32Sunset.jpg', '', '2015-06-26 11:27:04', '2015-06-26 11:27:04'),
(271, 85, 'god15-06-27-06-12-13Water lilies.jpg', '', '2015-06-27 04:12:43', '2015-06-27 04:12:43'),
(272, 85, 'god15-06-27-06-12-24Winter.jpg', '', '2015-06-27 04:12:43', '2015-06-27 04:12:43'),
(276, 81, 'god15-06-11-13-31-05Blue hills.jpg', '', '2015-07-03 11:22:22', '2015-07-03 11:22:22'),
(277, 81, 'god15-06-12-06-17-40Winter.jpg', '', '2015-07-03 11:22:22', '2015-07-03 11:22:22'),
(278, 81, 'god15-06-12-08-37-32Sunset.jpg', '', '2015-07-03 11:22:22', '2015-07-03 11:22:22'),
(279, 76, 'god15-06-11-13-31-05Blue hills.jpg', '', '2015-07-08 01:27:49', '2015-07-08 01:27:49'),
(280, 76, 'god15-06-12-06-17-40Winter.jpg', '', '2015-07-08 01:27:49', '2015-07-08 01:27:49'),
(281, 76, 'god15-06-12-08-37-32Sunset.jpg', '', '2015-07-08 01:27:49', '2015-07-08 01:27:49'),
(282, 77, 'god15-06-11-13-31-05Blue hills.jpg', '', '2015-07-13 08:30:18', '2015-07-13 08:30:18'),
(283, 77, 'god15-06-12-06-17-40Winter.jpg', '', '2015-07-13 08:30:18', '2015-07-13 08:30:18'),
(284, 77, 'god15-06-12-08-37-32Sunset.jpg', '', '2015-07-13 08:30:18', '2015-07-13 08:30:18'),
(285, 71, 'god15-06-11-13-31-05Blue hills.jpg', '', '2015-07-13 08:30:42', '2015-07-13 08:30:42'),
(286, 71, 'god15-06-12-06-17-40Winter.jpg', '', '2015-07-13 08:30:42', '2015-07-13 08:30:42'),
(287, 71, 'god15-06-12-08-37-32Sunset.jpg', '', '2015-07-13 08:30:42', '2015-07-13 08:30:42'),
(288, 74, 'god15-06-11-13-31-05Blue hills.jpg', '', '2015-07-13 08:32:58', '2015-07-13 08:32:58'),
(289, 74, 'god15-06-12-06-17-40Winter.jpg', '', '2015-07-13 08:32:58', '2015-07-13 08:32:58'),
(290, 74, 'god15-06-12-08-37-32Sunset.jpg', '', '2015-07-13 08:32:58', '2015-07-13 08:32:58'),
(291, 80, 'god15-06-11-13-31-05Blue hills.jpg', '', '2015-07-13 08:54:53', '2015-07-13 08:54:53'),
(292, 80, 'god15-06-12-06-17-40Winter.jpg', '', '2015-07-13 08:54:53', '2015-07-13 08:54:53'),
(293, 80, 'god15-06-12-08-37-32Sunset.jpg', '', '2015-07-13 08:54:53', '2015-07-13 08:54:53');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pointer`
--
ALTER TABLE `pointer`
  ADD CONSTRAINT `FK_pointer` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_pointer2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pointer_photo`
--
ALTER TABLE `pointer_photo`
  ADD CONSTRAINT `FK_pointer_photo` FOREIGN KEY (`pointer_id`) REFERENCES `pointer` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
