<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Pointer extends Model {

	protected $table = "pointer";
	protected $fillable = ['id','group_id','pointer_age','pointer_period','category_id','pointer_name','pointer_kananame','pointer_address','pointer_longitude','pointer_latitude','pointer_curaddress','pointer_comment','pointer_extra','pointer_show'];
	public function getPointer($pointer_id)
    {
        $res = DB::table($this->table)->join('category','pointer.category_id','=','category.id')
        ->select('pointer.id','pointer.pointer_name','pointer.pointer_address','pointer.pointer_curaddress','pointer.pointer_show','category.id as category_id','category.category_name','pointer.pointer_longitude','pointer.pointer_latitude','pointer.pointer_longlatdisp','category.pointer_color')
        ->whereRaw("pointer.id=".$pointer_id)->get();
        return $res;
    }
	
}